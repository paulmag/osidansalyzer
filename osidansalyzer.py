# -*- coding: utf-8 -*-
"""
OSIDansalyzer
OSI Dans Analyzer

Tool for overviewing membership status is OSI Dans by analyzing a CSV-version
of the registration spreadsheet.

author: Paul Magnus Sørensen-Clark
email: paulmag91@gmail.com
organisation: OSI Dans
email: osidans-styret@osi.uio.no
website: http://www.osidans.no/
"""
__version__ = "0.6.1"

import sys
import csv
import re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('filename')
parser.add_argument('--all',     action='store_true')
parser.add_argument('--nodups', action='store_true')
parser.add_argument('--olddups', action='store_true')
parser.add_argument('--nobade', action='store_true')
parser.add_argument('--stat',  action='store_true')
parser.add_argument('--list',  action='store_true')
args = parser.parse_args()

infile = open(args.filename, 'r')
indata = csv.reader(infile)

YES = "Ja / Yes"
NO = "Nei / No"
LEAD = "Fører / Lead"
FOLLOW = "Følger / Follow"
COUPLE = "Hvilke kurs (pardans) ønsker du å følge høsten 2016? / What classes (couple dance) do you wish to attend autumn 2016? "
GROUP = "Hvilke kurs (gruppedans) ønsker du å følge høsten 2016? / What classes (group dance) do you wish to attend autumn 2016? "
SPLIT = " / "  # Splitter between English and Norwegian text.
APPROVED = "godkjent"
NOTAPPROVED = "ikke"
STUDENT = "Student"
NOTSTUDENT = "Ikke-student"

iTIME = 0
iNAME = 1
iEMAIL = 2
iEMAIL2 = 3
iPHONE = 4
iSO = ~5  # "Significant Other": dance partner
iSTUDENT = ~4
iLETTER = ~3
iSTATUS = ~0


"""Read in table data."""
header = indata.next()
kinds = []
courses = []
for i, field in enumerate(header):
    if field.startswith(COUPLE):
        kinds.append("couple")
    elif field.startswith(GROUP):
        kinds.append("group")
    else:
        kinds.append("personal")
    field = field.lstrip(COUPLE).lstrip(GROUP).split(SPLIT)[0].lstrip("[").rstrip("]")
    header[i] = re.sub('\(.*?\) ','', field)  # Remove parenthesis ().
    if kinds[~0] != "personal":
        courses.append(header[i])
table = []
for line in indata:
    table.append(line)


def count(include_waiting=args.all):
    """Count participants of each course."""
    coursecounts = []
    for i, kind in enumerate(kinds):
        if kind in ("couple", "group"):
            currentcount = [0, 0, 0]
            for j, person in enumerate(table):
                if not include_waiting:
                    if not person[iSTATUS].startswith(APPROVED): continue
                ans = table[j][i]
                if ans == LEAD:
                    currentcount[0] += 1
                    currentcount[2] += 1
                elif ans == FOLLOW:
                    currentcount[1] += 1
                    currentcount[2] += 1
                elif ans == YES:
                    currentcount[2] += 1
            coursecounts.append(currentcount)
    print "%19s    L  F  T" % ("")
    for i, val in enumerate(coursecounts):
        print "%-19s   %2d %2d %2d" % (courses[i], val[0], val[1], val[2])
    return coursecounts


def argsort(seq):
    """
    http://stackoverflow.com/questions/3382352/equivalent-of-numpy-argsort-in-basic-python/3382369#3382369
    by unutbu
    """
    return sorted(range(len(seq)), key=seq.__getitem__)


def make_lists(include_waiting=args.all):
    """Make lists of approved members of each course."""
    ii = 0
    for i, kind in enumerate(kinds):
        if kind in ("couple", "group"):
            print
            print "%3s%s" % ("", courses[ii])
            ii += 1
            indexlist = []
            namelist = []
            table2 = []
            for j, person in enumerate(table):
                if not include_waiting:
                    if not person[iSTATUS].startswith(APPROVED): continue
                ans = table[j][i]
                if ans in (LEAD, FOLLOW, YES):
                    indexlist.append(j)
                    namelist.append(person[iNAME])
            indices_sorted = argsort(namelist)
            for j in indices_sorted:
                table2.append(table[indexlist[j]])
            for j, person in enumerate(table2):
                print "%-38s" % (person[iNAME])


def statistics():
    """Find ratio of approved members and ratio of non-students."""
    n_student = 0
    n_notstudent = 0
    n_approved = 0
    n_notapproved = 0
    result = [[0, 0], [0, 0]]
    for row in table:
        if row[iSTATUS].startswith(APPROVED):
            if row[iSTUDENT].startswith(STUDENT): result[1][1] += 1
            elif row[iSTUDENT].startswith(NOTSTUDENT): result[1][0] += 1
        elif row[iSTATUS].startswith(NOTAPPROVED):
            pass
        else:
            if row[iSTUDENT].startswith(STUDENT): result[0][1] += 1
            elif row[iSTUDENT].startswith(NOTSTUDENT): result[0][0] += 1
    print "Approved:"
    print "  Total:      %3d" % sum(result[1])
    print "  Non-student:%3d (%4.1f %%)" \
        % (result[1][0], result[1][0] * 100. / sum(result[1]))
    print "Waiting list:"
    print "  Total:      %3d" % sum(result[0])
    print "  Non-student:%3d (%4.1f %%)" \
        % (result[0][0], result[0][0] * 100. / sum(result[0]))
    print "All:"
    print "  Total:      %3d" % (sum(result[0]) + sum(result[1]))
    print "  Non-student:%3d (%4.1f %%)" \
        % ( result[0][0] + result[1][0],
           (result[0][0] + result[1][0]) * 100. / (sum(result[0]) + sum(result[1]))
        )


def find_duplicates(only_new=(not args.olddups)):
    """Find duplicate members by looking for similar personal information."""
    duplicates = []
    if only_new:
        found = []
        for a, row_a in enumerate(table):
            if row_a[iSTATUS].startswith(APPROVED): continue
            for b, row_b in enumerate(table):
                if a == b or [a, b] in found: continue
                if (row_a[iNAME] == row_b[iNAME] or
                    row_a[iEMAIL] == row_b[iEMAIL]
                ):  # Identical name/email.
                    duplicates.append([
                        [row_a[iTIME]] + [row_a[iNAME]] +
                        [row_a[iEMAIL]] + [row_a[iPHONE]]   +
                        ["Partner: " + row_a[iSO]]   +
                        ["Newsletter: " + row_a[iLETTER].split()[0][:~0]] +
                        [row_a[iSTUDENT]] +
                        ["Status: " + row_a[iSTATUS]],
                        [row_b[iTIME]] + [row_b[iNAME]] +
                        [row_b[iEMAIL]] + [row_b[iPHONE]]   +
                        ["Partner: " + row_b[iSO]]   +
                        ["Newsletter: " + row_b[iLETTER].split()[0][:~0]] +
                        [row_b[iSTUDENT]] +
                        ["Status: " + row_b[iSTATUS]],
                    ])
                    found.append([a, b])
                    found.append([b, a])
    else:
        for a, row_a in enumerate(table):
            for b, row_b in enumerate(table[a+1:]):
                if (row_a[iNAME] == row_b[iNAME] or
                    row_a[iEMAIL] == row_b[iEMAIL] or
                    row_a[iPHONE][~7:] == row_b[iPHONE][~7:]
                ):  # Identical name/email/phone.
                    duplicates.append([
                        [row_a[iTIME]] + [row_a[iNAME]] +
                        [row_a[iEMAIL]] + [row_a[iPHONE]]   +
                        ["Partner: " + row_a[iSO]]   +
                        ["Newsletter: " + row_a[iLETTER].split()[0][:~0]] +
                        [row_a[iSTUDENT]] +
                        ["Status: " + row_a[iSTATUS]],
                        [row_b[iTIME]] + [row_b[iNAME]] +
                        [row_b[iEMAIL]] + [row_b[iPHONE]]   +
                        ["Partner: " + row_b[iSO]]   +
                        ["Newsletter: " + row_b[iLETTER].split()[0][:~0]] +
                        [row_b[iSTUDENT]] +
                        ["Status: " + row_b[iSTATUS]],
                    ])
    for pair in duplicates:
        print "%35s" % "Duplicate:"
        for field_a, field_b in zip(pair[0], pair[1]):
            print "%-38s   %-38s" % (field_a, field_b)
    return duplicates


def find_bad_emails():
    """Find entries that have entered 2 different email addresses."""
    for i, row in enumerate(table):
        if row[iEMAIL] != row[iEMAIL2]:  # email doesn't match
            print "%35s" % "Bad email:"
            print row[iNAME]
            print row[iEMAIL]
            print row[iEMAIL2]
            print row[iPHONE]
            print "Status: " + row[iSTATUS]



"""Run all functions."""
count()
if not args.nodups:
    find_duplicates()
if not args.nobade:
    find_bad_emails()
if args.stat:
    statistics()
if args.list:
    make_lists()
